# Pipeline infrastructure(in Terraform)

This repo contains the Terraform code to deploy a CodePipeline consisting of a CodeCommit source, CodeBuild project, and optional S3 notification event tied to a Lambda Function. Changes made to the linked CodeCommit repository are watched by the pipeline, which will in turn invoke CodeBuild to perform the desired build steps. If output to S3 is made, and the S3 notification enabled, a Lambda function will trigger that can send a notification to a user, trigger another workflow, publish code to another Lambda function or Layer, etc.

## Prerequisites

You'll need to have a few things prepared in order to set up everything required for this project.

* terraform (tested with v11.11) installed wherever it's being used(local computer, build environment)
* An Amazon Web Services account(ideally under the free tier so it doesn't cost you anything to try!)
* The credentials for an IAM user with programmatic access to said AWS account(note that this user needs permission to create all of the necessary resources - https://aws.amazon.com/iam/details/manage-permissions/)

## Setup

### Variables
The variables used in your environment's `.tfvars` file are listed below.

#### Required
* **artifact_bucket**: Name of the S3 bucket to store artifacts in
* **artifact_filename**: The filename to use for the artifact, without the .zip extension
* **project_name**: The name of your project - acts as a namespace(prefix) for deployed resources.
* **repo_name**: Name of the source repo, i.e. 'my_app'


#### Optional
* **artifact_path**: S3 path where CodePipeline will deposit build artifacts(do not add a trailing slash).
* **aws_region**: The region into which your infrastructure is being deployed. Default: `us-west-2`.
* **aws_profile**: AWS Credentials profile name. Default: `default`).
* **build_image**: Docker image to be used by CodeBuild. Default: `aws/codebuild/eb-python-3.4-amazonlinux-64:2.1.6`.
* **buildspec_file**: Path and filename of buildspec file. Default: `buildspec.yml`.
* **compute_type**: The size of container to be used by CodeBuild. One of: `[BUILD_GENERAL1_SMALL, BUILD_GENERAL1_MEDIUM, BUILD_GENERAL1_LARGE]`. Default: `BUILD_GENERAL1_SMALL`.
* **environment_variables**: A list of objects to be used as env vars in CodeBuild. i.e. [{
    "name"  = "NONE"
    "value" = "TRUE"
  }]. Default: 
* **publish_lambda_arn**: ARN of Lambda function to call on artifact deployment to S3(only needed if `s3_notification_enabled` is `true`).
* **s3_notification_enabled**: Boolean indicating whether to enable the S3 notification module. Default: `false`
* **source_branch**: Upstream repo branch to monitor. Default: `master`.
* **tags**: Additional tags to add to resources(passed in as a map, i.e. {'tag1': 'value1', 'tag2': 'value2'}). Default: `{}`.


## Configuration

## License
[MIT](https://choosealicense.com/licenses/mit/)