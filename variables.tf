variable "aws_region" {
  type        = "string"
  default     = "us-west-2"
  description = "Specifies the region that the S3 bucket will be created in."
}

variable "project_name" {
  type        = "string"
  description = "The name of your project - acts as a namespace(prefix) for deployed resources."
}

variable "compute_type" {
  type        = "string"
  default     = "BUILD_GENERAL1_SMALL"
  description = "Compute type for the build container."
}

variable "build_image" {
  type        = "string"
  default     = "aws/codebuild/eb-python-3.4-amazonlinux-64:2.1.6"
  description = "String containing the image to use for the build container."
}

variable "environment_variables" {
  type        = "list"
  default     = [{
    "name"  = "NONE"
    "value" = "TRUE"
  }]
  description = "A list of maps(key-value pairs) to be added as environment variables."
}

variable "artifact_bucket" {
  type        = "string"
  description = "Name of the S3 bucket to store artifacts in."
}

variable "artifact_path" {
  type        = "string"
  default     = ""
  description = "Path to store artifacts in S3 Bucket."
}

variable "artifact_filename" {
  type        = "string"
  description = "The filename to use for the artifact, without the .zip extension."
}

variable "source_artifact_key" {
  type        = "string"
  description = "The filename to use for the source artifact, i.e. source.zip"
}

variable "buildspec_file" {
  type        = "string"
  default     = "buildspec.yml"
  description = "Path and filename of buildspec file(defaults to 'buildspec.yml')."
}

variable "aws_profile" {
  type        = "string"
  description = "AWS Credentials profile name."
  default     = "default"
}

variable "environment" {
  type        = "string"
  default     = "dev"
  description = "Name of the environment"
}

variable "deployment_function_name" {
  type        = "string"
  default     = "build_deployment_lambda"
  description = "Name of the lambda function to invoke after the build stage"
}

variable "deployment_function_user_params" {
  type        = "string"
  default     = ""
  description = "A json string of user-defined parameters to pass to the deployment lambda"
}
