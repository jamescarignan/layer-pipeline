variable "artifact_bucket" {
  type        = "string"
  description = "Name of the S3 bucket to store artifacts in"
}

variable "artifact_path" {
  type        = "string"
  default     = ""
  description = "Path to store artifacts in S3 Bucket"
}

variable "artifact_filename" {
  type        = "string"
  description = "The filename to use for the artifact"
}

variable "source_artifact_key" {
  type        = "string"
  description = "The filename to use for the source artifact, i.e. source.zip"
}

variable "codebuild_name" {
  type        = "string"
  description = "Name of the CodeBuild project"
}

variable "project_name" {
  type        = "string"
  default     = "test"
  description = "Name of the project"
}

variable "aws_region" {
  type        = "string"
  default     = "us-west-2"
  description = "Specifies the region that the S3 bucket will be created in."
}

variable "deployment_function_name" {
  type        = "string"
  default     = "build_deployment_lambda"
  description = "Name of the lambda function to invoke after the build stage"
}

variable "deployment_function_user_params" {
  type        = "string"
  default     = ""
  description = "A json string of user-defined parameters to pass to the deployment lambda"
}
