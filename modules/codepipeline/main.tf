data "aws_caller_identity" "current" {}

resource "aws_iam_role" "codepipeline_role" {
  name = "${var.project_name}-codepipeline-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codepipeline.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "codepipeline_policy" {
  name = "${var.project_name}-codepipeline-policy"
  role = "${aws_iam_role.codepipeline_role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect":"Allow",
      "Action": "*",
      "Resource": [
        "arn:aws:s3:::${var.artifact_bucket}",
        "arn:aws:s3:::${var.artifact_bucket}/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": "*",
      "Resource": "arn:aws:codebuild:${var.aws_region}:${data.aws_caller_identity.current.account_id}:project/${var.codebuild_name}"
    },
    {
      "Action": "lambda:InvokeFunction",
      "Resource": "arn:aws:lambda:${var.aws_region}:${data.aws_caller_identity.current.account_id}:function:${var.deployment_function_name}",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_codepipeline" "codepipeline" {
  name     = "${var.project_name}-pipeline"
  role_arn = "${aws_iam_role.codepipeline_role.arn}"

  artifact_store {
    location = "${var.artifact_bucket}"
    type     = "S3"
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "S3"
      version          = "1"
      output_artifacts = ["${var.project_name}"]

      configuration = {
        S3Bucket       = "${var.artifact_bucket}"
        S3ObjectKey    = "${var.source_artifact_key}"
      }
    }
  }

  stage {
    name = "Build"

    action {
      name            = "Build"
      category        = "Build"
      owner           = "AWS"
      provider        = "CodeBuild"
      input_artifacts = ["${var.project_name}"]
      output_artifacts = ["packaged"]
      version         = "1"

      configuration = {
        ProjectName = "${var.project_name}-codebuild"
      }
    }
  }

  stage {
    name = "Deploy"

    action {
      name            = "Deploy"
      category        = "Invoke"
      owner           = "AWS"
      provider        = "Lambda"
      version         = "1"
      input_artifacts = ["packaged"]

      configuration = {
        FunctionName    = "${var.deployment_function_name}"
        UserParameters  = "${var.deployment_function_user_params}"
      }
    }
  }
}

resource "aws_lambda_permission" "allow_codepipeline" {
  statement_id  = "AllowExecutionFromCodePipeline"
  action        = "lambda:InvokeFunction"
  function_name = "${var.deployment_function_name}"
  principal     = "codepipeline.amazonaws.com"
  source_arn    = "${aws_codepipeline.codepipeline.arn}"
}
