locals {
  common_tags = {
    environment  = "${var.environment}"
    project_name = "${var.project_name}"
  }
}

resource "aws_iam_role" "codebuild" {
  name = "${var.project_name}-codebuild-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codebuild.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "codebuild" {
  name    = "${var.project_name}-codebuild-policy"
  role    = "${aws_iam_role.codebuild.name}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:*"
      ],
      "Resource": [
        "arn:aws:s3:::${var.artifact_bucket}",
        "arn:aws:s3:::${var.artifact_bucket}/*"
      ]
    }
  ]
}
POLICY
}

resource "aws_codebuild_project" "codebuild" {
  name              = "${var.project_name}-codebuild"
  description       = "Build project for ${var.project_name}"
  build_timeout     = "5"
  service_role      = "${aws_iam_role.codebuild.arn}"
  tags              = "${local.common_tags}"

  artifacts {
    type            = "CODEPIPELINE"
  }

  environment {
    compute_type    = "${var.compute_type}"
    image           = "${var.build_image}"
    type            = "LINUX_CONTAINER"

    environment_variable = [
      "${var.environment_variables}"
    ]
  }

  source {
    type            = "CODEPIPELINE"
  }
}
