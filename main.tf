module "codebuild" {
  source                  = "modules/codebuild"
  project_name            = "${var.project_name}"
  aws_region              = "${var.aws_region}"

  # https://docs.aws.amazon.com/codebuild/latest/userguide/build-env-ref-available.html
  build_image             = "${var.build_image}"
  compute_type            = "${var.compute_type}"
  artifact_bucket         = "${var.artifact_bucket}"
}

module "codepipeline" {
  source                  = "modules/codepipeline"
  project_name            = "${var.project_name}"
  aws_region              = "${var.aws_region}"
  codebuild_name          = "${module.codebuild.codebuild_name}"
  artifact_bucket         = "${var.artifact_bucket}"
  artifact_path           = "${var.artifact_path}"
  artifact_filename       = "${var.artifact_filename}"
  source_artifact_key     = "${var.source_artifact_key}"
  deployment_function_name = "${var.deployment_function_name}"
  deployment_function_user_params = "${var.deployment_function_user_params}"
}
